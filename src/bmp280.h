#include "spi.h"

#define BMP_PRES_MSB                    0xF7    // 3 Register
#define BMP_COMP_START                  0x8E    // 18 Register
#define BMP_CONFIG_REG                  0x75    // Standby and Filter Register
#define BMP_CONFIG                      0x28    // Standby = 001 (62ms), IIR Filter = 010 (4x), 3Wire-SPI = 00
#define BMP_CTRL_MEAS_REG               0x74    // Oversampling Temperature, Pressure and Mode Selection
#define BMP_CTRL_MEAS                   0x2F    // OS-Temp = 001 (1x), OS-Pressure = 011 (4x), Normal-Mode (11)

#define SPI_CS_PRES 8

void bmp_get_pres(uint8_t *rx);
void bmp_get_calibration_data(uint8_t *rx);
void bmp_init(void);