#include "nrf_drv_saadc.h"
#include "segger_rtt.h"

#define SAMPLES_IN_BUFFER 1

void saadc_callback(nrf_drv_saadc_evt_t const * p_event);
void saadc_init(void);
void saadc_sampling_trigger(void);
