#include "watchdog.h"

nrf_drv_wdt_channel_id m_channel_id;

void init_watchdog()
{
  uint32_t err_code = NRF_SUCCESS;
  if ( !nrf_wdt_started() )
  {
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    config.behaviour = NRF_WDT_BEHAVIOUR_RUN_SLEEP_HALT;
    config.reload_value = 15000;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
    nrf_drv_wdt_enable();
  }
}

void wdt_event_handler(void)
{
   nrf_drv_gpiote_out_toggle(17);
}

void watchdog_feed()
{
  nrf_drv_wdt_channel_feed(0);
}