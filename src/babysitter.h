#include "i2c.h"


#define BQ27441_CONTROL_REGISTER 0x00
#define BQ27441_COMMAND_VOLTAGE	0x04 // Voltage()
#define BQ27441_COMMAND_FLAGS	0x06 // Flags()
#define BQ27441_CONTROL_DEVICE_TYPE	0x01
#define BQ27441_SET_CFGUPDATE 0x13
#define BQ27441_ID_STATE 82
#define BQ27441_EXTENDED_CONTROL 0x61
#define BQ27441_EXTENDED_DATACLASS	0x3E
#define BQ27441_EXTENDED_DATABLOCK	0x3F
#define BQ27441_EXTENDED_BLOCKDATA	0x40 // BlockData()
#define BQ27441_EXTENDED_CHECKSUM	0x60 // BlockDataCheckSum()
#define BQ27441_CONTROL_SOFT_RESET	0x42
#define BQ27441_DESIGN_CAPACITY         100
#define BQ27441_COMMAND_DESIGN_CAPACITY 0x3C
#define BQ27441_COMMAND_NOM_CAPACITY	0x08 // NominalAvailableCapacity()
#define BQ27441_COMMAND_AVG_CURRENT	0x10 // AverageCurrent()
#define BQ27441_COMMAND_AVAIL_CAPACITY	0x0A // FullAvailableCapacity()
#define BQ27441_COMMAND_REM_CAPACITY	0x0C // RemainingCapacity()
#define BQ27441_COMMAND_FULL_CAPACITY	0x0E // FullChargeCapacity()
#define BQ27441_COMMAND_AVG_POWER	0x18 // AveragePower()
#define BQ27441_COMMAND_SOH	0x20 // StateOfHealth()
#define BQ27441_COMMAND_REM_CAP_UNFL	0x28 // RemainingCapacityUnfiltered()
#define BQ27441_COMMAND_SOC	0x1C // StateOfCharge()

#define BQ27441_FLAG_CFGUPMODE	(1<<4)
#define BQ72441_I2C_TIMEOUT 2000

#define BB_WRITE_ADR	(0xAAu >> 1)
#define BB_READ_ADR 	(0xABu >> 1)

uint8_t bb_getStateOfCharge(void);
uint16_t bb_getDesignCapacity(void);
void bb_deviceId(uint8_t *dev_id);
void bb_shutdown(void);
void bb_unseal(void);
void bb_seal(void);
void bb_setCapacity(uint16_t capacity);
void bb_writeExtendedData(uint8_t classID, uint8_t offset, uint8_t * data, uint8_t len);
void bb_blockDataControl(void);
void bb_blockDataClass(uint8_t classID);
void bb_blockDataOffset(uint8_t offset);
/* Read all 32 bytes of the loaded extended data and compute a 
   checksum based on the values.*/
uint8_t bb_computeBlockChecksum(void);
/* Read the current checksum using BlockDataCheckSum() */
uint8_t bb_blockDataChecksum(void);

uint16_t bb_getCapacity(capacity_measure type);

uint16_t bb_readWord(uint8_t subadress);
/* Use BlockData() to write a byte to an offset of the loaded data */
void bb_writeBlockData(uint8_t offset, uint8_t data);
/* Use the BlockDataCheckSum() command to write a checksum value */
void bb_writeBlockChecksum(uint8_t csum);

bool bb_sendCommand(uint8_t subAddress, uint8_t *data, uint8_t length);
bool bb_readBytes(uint8_t subAddress, uint8_t *data, uint8_t length);

uint16_t bb_readFlags(void);
uint8_t bb_readBlockData(uint8_t offset);
bool bb_softreset(void);
void bb_hardreset(void);