#include "babysitter.h"

uint8_t bb_getStateOfCharge(void)
{
  uint16_t stateOfCharge;
  stateOfCharge = bb_readWord(BQ27441_COMMAND_SOC);
  return stateOfCharge & 0xFF;
}

uint16_t bb_getDesignCapacity(void)
{
  uint16_t designCapacity;
  designCapacity = bb_readWord(BQ27441_COMMAND_DESIGN_CAPACITY);
  return designCapacity;
}

void bb_deviceId(uint8_t *dev_id)
{ uint8_t tx[2] = {BQ27441_CONTROL_DEVICE_TYPE,0x00};
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
  bb_readBytes(BQ27441_CONTROL_REGISTER,dev_id,2);
}

void bb_setCapacity(uint16_t capacity)
{ 
  uint8_t capacityData[2];
  capacityData[0] = capacity >> 8;
  capacityData[1] = capacity & 0x00FF;  
  bb_writeExtendedData(BQ27441_ID_STATE, 10, capacityData, 2);
  
}

void bb_writeExtendedData(uint8_t classID, uint8_t offset, uint8_t * data, uint8_t len)
{ uint8_t tx[2] = {BQ27441_SET_CFGUPDATE,0x00};
  uint8_t oldCsum = 0;
  uint8_t newCsum = 0;
  /* 3000mV terminate Voltage */
  uint8_t terminateVoltage[2] = { 0x0B , 0xB8 };
  /* unseal Babysitter */
  bb_unseal();
  /* set Babysitter to config mode */
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
  nrf_delay_ms(10);
  /* enable Block Data Control */
  bb_blockDataControl();
  /* select Block Data Class */
  bb_blockDataClass(classID);
  /* Calculating / sending offset */
  bb_blockDataOffset(offset/32);

  oldCsum = bb_blockDataChecksum();
  
  for (int i = 0; i < len; i++)
  {
    /* Write to offset, mod 32 if offset is greater than 32
      The blockDataOffset above sets the 32-bit block */
    bb_writeBlockData((offset % 32) + i, data[i]);
    bb_writeBlockData((16 % 32) + i, terminateVoltage[i]);
  }

  newCsum = bb_computeBlockChecksum(); // Compute the new checksum
  bb_writeBlockChecksum(newCsum);
  while(!bb_softreset());
  bb_seal();
}

bool bb_softreset()
{
  uint16_t timeout = BQ72441_I2C_TIMEOUT;
  uint8_t tx[2] = {BQ27441_CONTROL_SOFT_RESET , 0x00};
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
  while ((--timeout) && ((bb_readFlags() & BQ27441_FLAG_CFGUPMODE)))
   nrf_delay_ms(1);
  if (timeout > 0 )
    return true;
  else
    return false;

  
}

void bb_writeBlockChecksum(uint8_t csum)
{
  bb_sendCommand(BQ27441_EXTENDED_CHECKSUM,&csum,1);
}

void bb_writeBlockData(uint8_t offset, uint8_t data)
{
  bb_sendCommand(offset + BQ27441_EXTENDED_BLOCKDATA,&data,1);
}

void bb_blockDataControl(void)
{
  uint8_t tx = 0x00;
  bb_sendCommand(BQ27441_EXTENDED_CONTROL,&tx,1);
}

void bb_blockDataClass(uint8_t classID)
{
  bb_sendCommand(BQ27441_EXTENDED_DATACLASS,&classID,1);
}

void bb_blockDataOffset(uint8_t offset)
{
  bb_sendCommand(BQ27441_EXTENDED_DATABLOCK,&offset,1);
}


uint8_t bb_computeBlockChecksum(void)
{
  /* Data-Buffer */
  uint8_t data[32];
  /* checksum */
  uint8_t csum = 0;
  /* read 32 Byte from IC */
  bb_readBytes(BQ27441_EXTENDED_BLOCKDATA,data,32);
  /* calculating checksum */
  for (int i=0; i<32; i++)
  {
    csum += data[i];
  }
  csum = 255 - csum;
  
  return csum;
}

uint8_t bb_blockDataChecksum(void)
{
    uint8_t csum;
    /* send register to IC for subsequent reading */
    bb_readBytes(BQ27441_EXTENDED_CHECKSUM,&csum,1);
    return csum;
}

uint16_t bb_getCapacity(capacity_measure type)
{
	uint16_t capacity = 0;
	switch (type)
	{
	case REMAIN:
                
		capacity = bb_readWord(BQ27441_COMMAND_REM_CAPACITY);
		break;
        case REMAIN_UF:
		capacity = bb_readWord(BQ27441_COMMAND_REM_CAP_UNFL);
                break;
	case FULL:
		capacity = bb_readWord(BQ27441_COMMAND_FULL_CAPACITY);
		break;
	case AVAIL:
		capacity = bb_readWord(BQ27441_COMMAND_NOM_CAPACITY);
		break;
	case AVAIL_FULL:
		capacity = bb_readWord(BQ27441_COMMAND_AVAIL_CAPACITY);
		break;

	}
	
	return capacity;
}

uint16_t bb_readWord(uint8_t subadress)
{
  uint8_t data[2];
  i2c_read(BB_READ_ADR,subadress,data,2);
  return ((uint16_t) data[1] << 8) | data[0];
}

void bb_shutdown()
{
  bb_unseal();
  uint8_t tx[3] = {BQ27441_CONTROL_REGISTER,0x1B,0x00};
  i2c_send(BB_WRITE_ADR,tx,3);
  tx[1] = 0x1C;
  i2c_send(BB_WRITE_ADR,tx,3);
}

void bb_unseal()
{ uint8_t tx[2] = {0x80,0x00};
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
}

void bb_seal()
{ uint8_t tx[2] = {0x20,0x00};
  bb_sendCommand(BQ27441_CONTROL_REGISTER,tx,2);
}

bool bb_sendCommand(uint8_t subAddress, uint8_t *data, uint8_t length)
{
  
  uint8_t command[length+1];
  command[0]=subAddress;
  for (int i = 0; i<length; i++)
    command[i+1] = data[i];
  if(i2c_send(BB_WRITE_ADR,command, length+1))
      return true;
    else
      return false;
}

bool bb_readBytes(uint8_t subAddress, uint8_t *data, uint8_t length)
{
    if(i2c_read(BB_READ_ADR, subAddress, data, length))
      return true;
    else
      return false;
}

uint16_t bb_readFlags(void)
{
	return bb_readWord(BQ27441_COMMAND_FLAGS);
}

uint8_t bb_readBlockData(uint8_t offset)
{
	uint8_t ret;
	uint8_t address = offset + BQ27441_EXTENDED_BLOCKDATA;
	bb_readBytes(address, &ret, 1);
	return ret;
}

void bb_hardreset()
{
  bb_unseal();
  uint8_t tx[3] = {BQ27441_CONTROL_REGISTER,0x41,0x00};
  i2c_send(BB_WRITE_ADR,tx,3);
  nrf_delay_ms(100);

}