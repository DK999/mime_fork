#include "i2c.h"

static const nrf_drv_twi_t i2c_interface = NRF_DRV_TWI_INSTANCE(1);
static volatile bool i2c_xfer_done = false;

void i2c_handler(nrf_drv_twi_evt_t const * p_event, void * p_context)
{
	
	i2c_xfer_done = true;
}

void wait_for_I2C(void)
{
	while (!i2c_xfer_done){};
	i2c_xfer_done = false;
}
void i2c_enable (void)
{
	nrf_drv_twi_enable(&i2c_interface);
}

void i2c_disable (void)
{
	nrf_drv_twi_disable(&i2c_interface);
}

void i2c_init (void)
{
    ret_code_t err_code;
    
    const nrf_drv_twi_config_t bb_config = {
       .scl                = SCL_PIN,
       .sda                = SDA_PIN,
       .frequency          = I2C_FREQ,
       .interrupt_priority = APP_IRQ_PRIORITY_LOW
    };
    
    err_code = nrf_drv_twi_init(&i2c_interface, &bb_config, i2c_handler, NULL);
    APP_ERROR_CHECK(err_code);
    
    nrf_drv_twi_enable(&i2c_interface);
}

bool i2c_send(uint8_t address, uint8_t *data, uint8_t length)
{
  uint32_t err_code;
  err_code = nrf_drv_twi_tx(&i2c_interface, address, data, length, false);
  wait_for_I2C();
    if(err_code == NRF_SUCCESS)
      return true;
    else
      return false;
}

bool i2c_read(uint8_t address, uint8_t reg, uint8_t *data, uint8_t length)
{
  uint32_t err_code;
  err_code = nrf_drv_twi_tx(&i2c_interface, address, &reg, 1, true);
  wait_for_I2C();
  err_code = nrf_drv_twi_rx(&i2c_interface, address, data, length);
  wait_for_I2C();
    if(err_code == NRF_SUCCESS)
      return true;
    else
      return false;
}