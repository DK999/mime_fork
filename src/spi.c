#include "spi.h"

static nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(0); 
static volatile bool spi_xfer_done = false;
extern ble_svc_bmx bmx_service;



void spi_event_handler(nrf_drv_spi_evt_t const * p_event)
{
    spi_xfer_done = true;
}

void wait_for_SPI(void)
{
	while (!spi_xfer_done){};
	spi_xfer_done = false;
}

void spi_init(void)
{
	nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG(0);
	/* FREQUENCY CHANGED TO FREQ_8M IN NRF_DRV_SPI */
	nrf_drv_spi_init(&spi, &spi_config, spi_event_handler);
	NRF_GPIO->DIRSET |= (1 << SPI_CS_GYRO) | ( 1 << SPI_CS_ACC ) | ( 1 << SPI_CS_MAG ) | ( 1 << SPI_CS_PRES );
	NRF_GPIO->OUTSET |= (1 << SPI_CS_GYRO) | ( 1 << SPI_CS_ACC ) | ( 0 << SPI_CS_MAG ) | ( 1 << SPI_CS_PRES );
}

void spi_transfer(uint8_t *tx, uint8_t tx_length, uint8_t *rx, uint8_t rx_length, uint8_t SS)
{
    nrf_gpio_pin_clear(SS);
    nrf_drv_spi_transfer(&spi, tx, tx_length, rx, rx_length);
    wait_for_SPI();
    nrf_gpio_pin_set(SS);
}

void spi_enable(void)
{
	nrf_spi_enable(spi.p_registers);
}

void spi_disable(void)
{
	nrf_spi_disable(spi.p_registers);
}


