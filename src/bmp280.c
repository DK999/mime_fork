#include "bmp280.h"

uint8_t tx_spi[2],rx_spi[2];

void bmp_get_pres(uint8_t *rx)
{
	tx_spi[0]=BMP_PRES_MSB;
	/*Get Pressure Data */
        spi_transfer(tx_spi, 1, rx , 4 ,SPI_CS_PRES);
}

void bmp_get_calibration_data(uint8_t *rx)
{
	tx_spi[0]=BMP_COMP_START;
	/* Get Calibration Data */
        spi_transfer(tx_spi, 1, rx , 19 ,SPI_CS_PRES);
}

void bmp_init(void)
{
    tx_spi[0]=BMP_CONFIG_REG;       // Configure Standby-Time and Filter
    tx_spi[1]=BMP_CONFIG;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_PRES);
    tx_spi[0]=BMP_CTRL_MEAS_REG;    // Configure Oversampling and set to Normal Mode
    tx_spi[1]=BMP_CTRL_MEAS;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_PRES);
}