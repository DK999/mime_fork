#include "ble_inits.h"
#include "timers.h"
#include "services.h"
#include "bmx055.h"
#include "bmp280.h"
#include "babysitter.h"
#include "battery.h"
#include "watchdog.h"
#include "nrf_drv_gpiote.h"

#define LED_OUT 0
#define WATCHDOG 0
#define DEBUG 0
#define TIME_TILL_SLEEP 60
#define BB_LED_RED 31
#define BB_LED_BLUE 30
#define BB_LED_GREEN 29
#define DK_LED1 17

ble_svc_bmx bmx_service;		// BMX Services
ble_svc_bmx_conf bmx_conf_service;	// BMX Config Services

uint8_t config[10] = {0x01,0x00,0x03,0x00,0,0,0,0,0,0},config_old[10] = {0,0,0,0,0,0,0,0,0,0};
uint8_t batt_sched;
uint8_t batt_val[2];
uint8_t pressure_calib[19];
volatile uint16_t sleep_cnt;

bool run = false;			// Get measurements
bool wakeup = false;                    // Wake up BMX Sensor
bool sleep = false;			// Enter sleep
bool get_pres = false;			// Get pressure in sleep
bool connected = false;			// Device BLE connection status
                                   
/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

#ifdef LED_OUT
void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    nrf_drv_gpiote_out_toggle(17);
}


void init_input_int()
{ ret_code_t err_code;
  err_code = nrf_drv_gpiote_init();
  APP_ERROR_CHECK(err_code);
  
  nrf_drv_gpiote_out_config_t out_config = GPIOTE_CONFIG_OUT_SIMPLE(false);

    err_code = nrf_drv_gpiote_out_init(BB_LED_RED, &out_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_gpiote_out_init(BB_LED_BLUE, &out_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_gpiote_out_init(BB_LED_GREEN, &out_config);
    APP_ERROR_CHECK(err_code);

    nrf_drv_gpiote_out_clear(BB_LED_RED);
    nrf_drv_gpiote_out_clear(BB_LED_BLUE);
    nrf_drv_gpiote_out_clear(BB_LED_GREEN);
  nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
  in_config.pull = NRF_GPIO_PIN_PULLUP;

  err_code = nrf_drv_gpiote_in_init(BSP_BUTTON_0, &in_config, in_pin_handler);
  APP_ERROR_CHECK(err_code);

  nrf_drv_gpiote_in_event_enable(BSP_BUTTON_0, true);
}
#endif


/* BLE Service initialization
*/
static void svc_init(void)
{
	bmx_svc_init(&bmx_service);
	bmx_conf_svc_init(&bmx_conf_service);
}
/**@brief Function for the Power manager.
 */
static void power_manage(void)
{		
    // char str[100];
    // Pressure variables
    uint8_t pres[4]={0,0,0,0};						
    uint32_t pressure_old = 0;
    uint32_t pressure_new = 0;
    int16_t pressure_diff = 0;
    // Wait for event or enter sleep mode
    if ( sleep_cnt / config[0] < TIME_TILL_SLEEP )
    {	// Sleep and wait for event
            uint32_t err_code = sd_app_evt_wait();
            APP_ERROR_CHECK(err_code);
    }
    else
    {	
            // Stop Timer and Advertising
            measurement_timer_stop();	
            sd_ble_gap_adv_stop();
            nrf_delay_ms(100);
            // Get current pressure
            spi_enable();
            bmx_set_sleep();
            bmp_get_pres(pres);
            spi_disable();
            // Bitshift 2*8Bit
            pressure_old = ((uint8_t)pres[1] << 12) | ((uint8_t)pres[2] << 4) | (((uint8_t)pres[3] & 0xF0) >> 4);
            pressure_new = pressure_old;
            // activate sleep timer
            sleep_timer_start();
            sleep = true;
            while(sleep == true)
            {	// Sleep till Timer-Evt
              uint32_t err_code = sd_app_evt_wait();
              APP_ERROR_CHECK(err_code);
              #ifdef WATCHDOG
              watchdog_feed();
              #endif
              if(get_pres == true)
              {
                  // Get new pressure
                  spi_enable();
                  bmp_get_pres(pres);
                  spi_disable();
                  pressure_new = ((uint8_t)pres[1] << 12) | ((uint8_t)pres[2] << 4) | (((uint8_t)pres[3] & 0xF0) >> 4);
                      if ( (pressure_diff <= -20) || (pressure_diff >= 20))
                      {
                              sleep = false;
                      }
                      get_pres = false;
              }
            }
            sleep_timer_stop();
            sleep_cnt = 0;
            measurement_timer_start();
            
            uint32_t err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
            APP_ERROR_CHECK(err_code);
            
    }
}
/**@brief Function for checking new config for timer or BMX
 */
static void check_config()
{
  if(config[0] != config_old[0])
    {
      config_old[0] = config[0];
      measurement_timer_stop();
      wakeup_timer_stop();
      measurement_timer_start();
      wakeup_timer_start();
    }
     if(config[1] != config_old[1])
    {
      config_old[1] = config[1];
      bmx_set_gyro_res(config[1]);
    }
     if(config[2] != config_old[2])
    {
      config_old[2] = config[2];
      bmx_set_acc_res(config[2]);
    }
}

/**@brief Function for application main entry.
 */
int main(void)
{
    uint32_t err_code;
    bool erase_bonds;
    #ifdef DEBUG
    char str[100];
    #endif
    static uint8_t gyro[7],acc[7],mag[9],temp[2],pressure[4] = {0,0,0,0},stateOfCharge = 0;           // TEMP STORAGE
    static uint8_t sensordata[17]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  // ARRAY FOR BLUETOOTH SERVICE
    sleep_cnt = 0;
    batt_sched = 0;
    config_old[0]=config[0];     // TIMER
    config_old[1]=config[1];     // GYRO
    config_old[2]=config[2];     // ACC
    // ##### WAIT FOR SENSOR STARTUP! #####
    nrf_delay_ms(2);
     // ##### INIT INTERFACES #####
    spi_init();
    i2c_init();
    // ##### INIT DCDC CONVERTER #####
    sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
     // ##### INIT SENSORS #####
    
    //bmx_init_mag();
    bmx_init_gyro();
    bmx_init_acc();
    bmp_init();
    bb_hardreset();
    bb_setCapacity(BQ27441_DESIGN_CAPACITY);

    // ##### GET PRESSURE CALIBRATION #####
    bmp_get_calibration_data(pressure_calib);
    // ##### SHIFT SENSOR DATA #####
    for(int i = 0; i < 18; i++)
      pressure_calib[i]=pressure_calib[i+1];

    // ##### INIT TIMER! #####
    timers_init();
    // ##### INIT BLE! #####
    ble_stack_init();
    device_manager_init(erase_bonds);
    gap_params_init();
    advertising_init();	
    svc_init();
    conn_params_init();
    // ##### START WATCHDOG, TIMERS AND ADVERTISING! #####
    measurement_timer_start();
    err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
    #ifdef WATCHDOG
    init_watchdog();
    #endif
    #ifdef LED_OUT
    init_input_int();
    #endif
    /* ##### SET TX POWER! #####
    0 = 0dBm
    FC = -4dBm
    F8 = -8dBm
    F4 = -12dBm
    F0 = -16dBm
    EC = -20dBm
    D8 = -40dBm
    */
    err_code = sd_ble_gap_tx_power_set(0x00);
    APP_ERROR_CHECK(err_code);
    
    //bb_shutdown();
    // Enter main loop.
    for (;;)
    {   // ##### CALL PWR-MANAGE FOR SLEEP/DEEP SLEEP #####
        power_manage();
        if(run) // ##### RUN CASE TO GET AND SEND SENSORDATA #####
        {	// ##### RESET SLEEP-COUNTER! #####
                sleep_cnt = 0;
                // ##### ENABLE SPI, CONFIGURE SENSORS, GET DATA, SET TO SLEEP AND DISABLE SPI! #####
                spi_enable();
                check_config();
                bmx_get_accgyro(gyro,0);
                bmx_get_temp(temp);
                bmx_get_accgyro(acc,1);
                bmx_set_sleep();
                bmp_get_pres(pressure);
                spi_disable();
                // ##### BATTERY #####
                /* old implementation via ADC
                saadc_sampling_trigger();		//	Get ADC Value for Battery
                */
                
                if(bb_trigger())
                {
                  i2c_enable();
                  /* check if Battery config changed */
                  if(bb_getDesignCapacity() != BQ27441_DESIGN_CAPACITY)
                    bb_setCapacity(BQ27441_DESIGN_CAPACITY);
                  /* get State of Charge in % */
                  stateOfCharge = bb_getStateOfCharge();
                  #ifdef DEBUG
                  sprintf(str,"SoC | Volt | Current | CapF | CapR | PWR | SoH | DesCap \n");
                  SEGGER_RTT_WriteString(0, str);
                  sprintf(str," %d   %d    %d    %d     %d   %d   %d   %d\n",stateOfCharge,bb_readWord(BQ27441_COMMAND_VOLTAGE),bb_readWord(BQ27441_COMMAND_AVG_CURRENT),bb_readWord(BQ27441_COMMAND_FULL_CAPACITY),bb_readWord(BQ27441_COMMAND_REM_CAPACITY),bb_readWord(BQ27441_COMMAND_AVG_POWER),bb_readWord(BQ27441_COMMAND_SOH),bb_readWord(0x3C));
                  SEGGER_RTT_WriteString(0, str);
                  #endif
                  i2c_disable();
                  #ifdef LED_OUT
                  if(stateOfCharge >= 0x43) // 67%
                    nrf_drv_gpiote_out_set(BB_LED_GREEN);
                  else if(stateOfCharge >= 0x14) // 20%
                    nrf_drv_gpiote_out_set(BB_LED_BLUE);
                  else if(stateOfCharge < 0x14) // 20%
                    nrf_drv_gpiote_out_set(BB_LED_RED);
                  #endif
                }
                // ##### PREPARE SENSORDATA #####
                for(int i = 0; i < 6; i++)
                        sensordata[i]=gyro[i];
                for(int i = 0; i < 6; i++)
                        sensordata[i+6]=acc[i];
                sensordata[12]=temp[0];
                sensordata[13]=stateOfCharge;
                for(int i = 0; i < 3; i++)
                        sensordata[i+14]=pressure[i+1];

                // ##### UPDATE BLUETOOTH SERVICE #####
                sensors_char_update(&bmx_service,sensordata);
                run = false;
        }
        if(wakeup)  // ##### WAKE UP CASE TO ENABLE SENSORS #####
        {
          spi_enable();
          bmx_wakeup();
          spi_disable();
          wakeup = false;
        }
        if(config[3] == 1)  // ##### DROP DETECTION #####
        {
          measurement_timer_stop();
          wakeup_timer_stop();
          spi_enable();
          bmx_set_sleep();
          bmx_set_acc_int();
          spi_disable();
          wakeup = false;
          run = false;
          while(config[3] == 1)
          {
            
            #ifdef WATCHDOG   // Watchdog 
            watchdog_feed();
            #endif 
          }
          
        }
        #ifdef LED_OUT
        if(bb_ledOff())
        { /* Clear LED Output */
          nrf_drv_gpiote_out_clear(BB_LED_RED);
          nrf_drv_gpiote_out_clear(BB_LED_BLUE);
          nrf_drv_gpiote_out_clear(BB_LED_GREEN);
        }
        #endif
	#ifdef WATCHDOG   // Watchdog 
        watchdog_feed();
        #endif    
    }
}

