#ifndef TIMERS_H__
#define TIMERS_H__

#include "app_timer.h"
#include "services.h"
#include "spi.h"


#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          4                                          /**< Size of timer operation queues. */


APP_TIMER_DEF(measurement_timer_id);		// Define Timer for Sensor interval
APP_TIMER_DEF(sleep_timer_id);		// Define Timer for BMX055 interval
APP_TIMER_DEF(wakeup_timer_id);		// Define Timer for BMX055 interval
#define MEASUREMENT_TIMER_INTERVAL10     APP_TIMER_TICKS(100, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL9     APP_TIMER_TICKS(111, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL8     APP_TIMER_TICKS(125, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL7     APP_TIMER_TICKS(142, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL6     APP_TIMER_TICKS(166, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL5     APP_TIMER_TICKS(200, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL4     APP_TIMER_TICKS(250, APP_TIMER_PRESCALER) // 250 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL3     APP_TIMER_TICKS(333, APP_TIMER_PRESCALER) // 333 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL2     APP_TIMER_TICKS(500, APP_TIMER_PRESCALER) // 500 ms intervals, no Presacler
#define MEASUREMENT_TIMER_INTERVAL1     APP_TIMER_TICKS(1000, APP_TIMER_PRESCALER) // 1000 ms intervals, no Presacler
#define SLEEP_TIMER_INTERVAL1     APP_TIMER_TICKS(10000, APP_TIMER_PRESCALER) // 1000 ms intervals, no Presacler

#define WAKEUP_TIMER_INTERVAL10     APP_TIMER_TICKS(70, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL9     APP_TIMER_TICKS(81, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL8     APP_TIMER_TICKS(95, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL7     APP_TIMER_TICKS(112, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL6     APP_TIMER_TICKS(136, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL5     APP_TIMER_TICKS(170, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL4     APP_TIMER_TICKS(220, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL3     APP_TIMER_TICKS(303, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL2     APP_TIMER_TICKS(470, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler
#define WAKEUP_TIMER_INTERVAL1     APP_TIMER_TICKS(970, APP_TIMER_PRESCALER) // 200 ms intervals, no Presacler

#define BABYSITTER_INTERVAL 15

void timers_init(void);
void measurement_timer_start(void);
void measurement_timer_stop(void);
void measurement_timer_int_handler(void * p_context);
void wakeup_timer_int_handler(void * p_context);
void wakeup_timer_start(void);
void wakeup_timer_stop(void);
void sleep_timer_int_handler(void * p_context);
void sleep_timer_start(void);
void sleep_timer_stop(void);
bool bb_trigger(void);
bool bb_ledOff(void);
#endif // TIMERS_H__
