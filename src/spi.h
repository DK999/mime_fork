
#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include <string.h>
#include "services.h"
//#include "nrf_gpio.h"
#include "boards.h"
#include "segger_rtt.h"
#include "nrf_delay.h"
#include "calc.h"


#define LEFTSHIFT_ACC 4
#define RIGHTSHIFT_ACC 4
#define LEFTSHIFT_GYRO 8
#define RIGHTSHIFT_GYRO 0

#define SPI_CS_GYRO 5
#define SPI_CS_ACC 6
#define SPI_CS_MAG 7
#define SPI_CS_PRES 8





void spi_event_handler(nrf_drv_spi_evt_t const * p_event);
void spi_init(void);
void spi_enable(void);
void spi_disable(void);
void spi_transfer(uint8_t *tx, uint8_t tx_length, uint8_t *rx, uint8_t rx_length, uint8_t SS);



