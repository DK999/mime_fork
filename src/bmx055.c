#include "bmx055.h"

uint8_t tx_spi[2],rx_spi[2],rx_avg[35];

void bmx_get_temp(uint8_t *rx)
{
	tx_spi[0] = 0x88;
	
	spi_transfer(tx_spi, 1, rx, 2,SPI_CS_ACC);
	rx[0]=rx[1];
}

void bmx_get_accgyro(uint8_t *rx,uint8_t type)
{       
        int16_t Data_avg[3];
	tx_spi[0]=0x82;
	uint8_t CS,LSHIFT,RSHIFT;
	if(type==0)
	{
		CS=SPI_CS_GYRO;
		LSHIFT=LEFTSHIFT_GYRO;
		RSHIFT=RIGHTSHIFT_GYRO;
	}
	else
	{
		CS=SPI_CS_ACC;
		LSHIFT=LEFTSHIFT_ACC;
		RSHIFT=RIGHTSHIFT_ACC;
	}
		
	for(int i=0; i <35; i+=7)
	{
        spi_transfer(tx_spi, 1, rx_avg+i, 7,CS);
	}
	arithmetic_average(rx_avg,Data_avg,RSHIFT,LSHIFT);
	rx[0]=Data_avg[0] & 0xFF;
	rx[1]=(Data_avg[0] >> 8);
	rx[2]=Data_avg[1] & 0xFF;
	rx[3]=(Data_avg[1] >> 8);
	rx[4]=Data_avg[2] & 0xFF;
	rx[5]=(Data_avg[2] >> 8);	
}

void bmx_get_mag(uint8_t *rx)
{
	
	tx_spi[0]=BMX_MAG_PWR_CTRL2;
	tx_spi[1]=BMX_MAG_FORCE_MODE;
        /* Set MAG to forced Mode */
        spi_transfer(tx_spi, 2, rx , 0 ,SPI_CS_MAG);
	nrf_delay_ms(5);
	tx_spi[0]=BMX_MAG_DATA;
        /* Get MAG Data */
        spi_transfer(tx_spi, 1, rx , 9 ,SPI_CS_MAG);
}


void bmx_init_gyro(void)
{
    uint8_t BGW_Softreset[2] = {0x14,0xB6}, Range_Control[2]={0x0F,0x00}, Bandwith_Control[2]={0x10,0x00}, PowermodesCtrl[2]={0x11,0x00}, HBWCtrl[2]={0x13,0x00},rx;
    spi_transfer(BGW_Softreset, 2, &rx , 0 ,SPI_CS_GYRO);
    spi_transfer(Range_Control, 2, &rx , 0 ,SPI_CS_GYRO);
    spi_transfer(Bandwith_Control, 2, &rx , 0 ,SPI_CS_GYRO);
    spi_transfer(PowermodesCtrl, 2, &rx , 0 ,SPI_CS_GYRO);
    spi_transfer(HBWCtrl, 2, &rx , 0 ,SPI_CS_GYRO);
}


void bmx_init_acc(void)
{
    tx_spi[0]=BMX_ACC_BGW_RESET;
    tx_spi[1]=BMX_ACC_SOFTRESET;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
    
    tx_spi[0]=BMX_ACC_PMU_RANGE;
    tx_spi[1]=BMX_ACC_STD_RANGE;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);

    tx_spi[0]=BMX_ACC_PMU_BDWTH;
    tx_spi[1]=BMX_ACC_STD_BDWTH;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);

    tx_spi[0]=BMX_ACC_PMU_LPW1;
    tx_spi[1]=BMX_ACC_LPW1_NRML;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);

    tx_spi[0]=BMX_ACC_HBW_CTRL;
    tx_spi[1]=BMX_ACC_HBW_DATA_H;
    spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
}

void bmx_init_mag(void)
{
	tx_spi[0] = BMX_MAG_PWR_CTRL1;
	tx_spi[1] = 0x82; 		// Reset Mag, put into sleep mode
	spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_MAG);
	nrf_delay_ms(100);
	tx_spi[0] = BMX_MAG_PWR_CTRL1;
	tx_spi[1] = 0x01; 		// Set Power on
	spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_MAG);
	nrf_delay_ms(100);
	tx_spi[0] = BMX_MAG_REP_XY;
	tx_spi[1] = 0x01; 		// 3 Reps XY
	spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_MAG);
	tx_spi[0] = BMX_MAG_REP_Z;
	tx_spi[1] = 0x02; 		// 3 Reps Z
        spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_MAG);
}

void bmx_set_gyro_res(uint8_t range)
{
	if( (range != 0x00) && (range != 0x01) && (range != 0x02) && (range != 0x03) && (range != 0x04) )			// 2000?/s , 1000?/s, 500?/s, 250?/s, 125?/s Range
		{
			range = 0x00;
		}
		tx_spi[0]=0x0F;
		tx_spi[1]=range;
                spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_GYRO);
}

void bmx_set_acc_res(uint8_t range)
{		
  if( (range != 0x0C) && (range != 0x08) && (range != 0x05) && (range != 0x03) )			// 2g Range standard
          range = 0x03;
  tx_spi[0]=0x0F;
  tx_spi[1]=range;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
}

void bmx_set_sleep()
{
  tx_spi[0]=BMX_GYR_PMU_LPM1;
  tx_spi[1]=BMX_GYR_LPM1_SPND;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_GYRO);
  tx_spi[0]=BMX_ACC_PMU_LPW1;
  tx_spi[1]=BMX_ACC_LPW1_SPND;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
}

void bmx_wakeup()
{
  tx_spi[0]=BMX_GYR_PMU_LPM1;
  tx_spi[1]=BMX_GYR_LPM1_NRML;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_GYRO);
  tx_spi[0]=BMX_ACC_PMU_LPW1;
  tx_spi[1]=BMX_ACC_LPW1_NRML;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
  //nrf_delay_ms(35);
}

void bmx_set_acc_int()
{
  tx_spi[0]=BMX_ACC_PMU_LPW1;
  tx_spi[1]=BMX_ACC_LPW1_NRML;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
  tx_spi[0]=BMX_ACC_INT_EN1;
  tx_spi[1]=BMX_ACC_EN_HIGH_G;
  spi_transfer(tx_spi, 2, rx_spi , 0 ,SPI_CS_ACC);
}

