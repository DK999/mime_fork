#include "app_util_platform.h"
#include "nrf_drv_wdt.h"
#include "nrf_drv_gpiote.h"


void init_watchdog(void);
void wdt_event_handler(void);
void watchdog_feed(void);