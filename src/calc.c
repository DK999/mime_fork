#include "calc.h"

void arithmetic_average(uint8_t rx_data[],int16_t Data[],uint8_t rshift, uint8_t lshift)
{
	int32_t Tempx = 0;
	int32_t Tempy = 0;
	int32_t Tempz = 0;
	
	for ( int i = 0; i<35; i+=7)
	{
		Data[0] = (int8_t) rx_data[i+2]; 		// Cast MSB X to int8_t and make it signed in 16Bit Array
		Data[0] <<=lshift;							// Shift MSB X 4 Bits left to have space for LSB X
		Data[0] |= (rx_data[i+1] >> rshift);			// Do OR with first 4 Bits of LSB X
		Tempx += Data[0];						// Add Data to Temp-Var
		Data[1] = (int8_t) rx_data[i+4];		// Cast MSB Y to int8_t and make it signed in 16Bit Array
		Data[1] <<=lshift;							// Shift MSB X 4 Bits left to have space for LSB X
		Data[1] |= (rx_data[i+3] >> rshift);			// Do OR with first 4 Bits of LSB X
		Tempy += Data[1];						// Add Data to Temp-Var
		Data[2] = (int8_t) rx_data[i+6];		// Cast MSB Z to int8_t and make it signed in 16Bit Array
		Data[2] <<=lshift;							// Shift MSB Z 4 Bits left to have space for LSB Z
		Data[2] |= (rx_data[i+5] >> rshift);			// Do OR with first 4 Bits of LSB Z
		Tempz += Data[2];						// Add Data to Temp-Var
	}
	/*
	 * calculate arithmetic average and save into array
	 */
	Data[0] = (int16_t)Tempx / 5;
	Data[1] = (int16_t)Tempy / 5;
	Data[2] = (int16_t)Tempz / 5;
}
