#ifndef SERVICES_H__
#define SERVICES_H__

#include <stdbool.h>
#include <stdint.h>
#include "ble.h"
#include "ble_gatts.h"
#include "ble_dfu.h"

#include "dfu_app_handler.h"
#include "app_error.h"
#include "segger_rtt.h"
#include "timers.h"
#include "calc.h"
#include "ble_inits.h"

/* Defines for BMX-Services */
#define BLE_UUID_BASE              {{0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15,0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}} // 128-bit base UUID
#define BLE_UUID_BMX_SVC                0xF00D // Just a random, but recognizable value
#define BLE_UUID_BMX_CONF						0x1337

#define BLE_UUID_SENSORS_UUID          0x0010 // Just a random, but recognizable value
#define BLE_UUID_PRESSURE_CALIB_UUID   0x0011 // Just a random, but recognizable value

#define BLE_UUID_GYRO_CONF_UUID        0x001A // Just a random, but recognizable value
#define BLE_UUID_ACC_CONF_UUID         0x001B // Just a random, but recognizable value
#define BLE_UUID_MAG_CONF_UUID         0x001C // Just a random, but recognizable value
#define BLE_UUID_TIMER_CONF_UUID       0x001D
#define BLE_UUID_DROP_CONF_UUID        0x001E

/* Defines for Bootloader */
#ifdef BLE_DFU_APP_SUPPORT
#define DFU_REV_MAJOR                    0x00                                       /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                    0x01                                       /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                     ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)     /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START         0x000C                                     /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                   0xFFFF                                     /**< Max handle value in BLE. */
#endif

typedef struct
{
    uint16_t                    conn_handle;    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection).*/
    uint16_t                    service_handle; /**< Handle of Our Service (as provided by the BLE stack). */
    /* Handles for the characteristic attributes to struct */
    ble_gatts_char_handles_t    sensors_handle;
    ble_gatts_char_handles_t    bmp_calib_handle;
}ble_svc_bmx;

typedef struct
{
    uint16_t                    conn_handle;    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection).*/
    uint16_t                    service_handle; /**< Handle of Our Service (as provided by the BLE stack). */
    /* Handles for the characteristic attributes to struct */
    ble_gatts_char_handles_t    gyro_conf_handle;
    ble_gatts_char_handles_t    acc_conf_handle;
    ble_gatts_char_handles_t    mag_conf_handle;
    ble_gatts_char_handles_t    timer_conf_handle;
    ble_gatts_char_handles_t    drop_conf_handle;
}ble_svc_bmx_conf;

void bmx_svc_init(ble_svc_bmx *svc);
void bmx_conf_svc_init(ble_svc_bmx_conf *svc);
uint32_t bmx_add_chars(ble_svc_bmx *svc);
uint32_t bmx_conf_add_chars(ble_svc_bmx_conf *svc);

void ble_bmx_svc_on_ble_evt(ble_svc_bmx *svc, ble_evt_t * p_ble_evt);
void sensors_char_update(ble_svc_bmx *bmx_svc, uint8_t *sensors_val);
void bmp_calib_update(ble_svc_bmx *bmx_svc, uint8_t *sensors_val);
void set_config(ble_evt_t * p_ble_evt);
#endif // SERVICES_H__
