#include "timers.h"

extern ble_svc_bmx bmx_service;
extern bool run;
extern bool wakeup;
extern bool get_pres;
extern bool connected;
extern uint8_t config[10];
extern uint16_t sleep_cnt;
extern uint8_t batt_sched;
uint32_t babysitter_trigger = 0;
/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates application timers.
 */
void timers_init(void)
{
    uint32_t err_code;
    // Initialize 32KHz timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
    // Create BMX Timer
    err_code = app_timer_create(&measurement_timer_id, APP_TIMER_MODE_REPEATED, measurement_timer_int_handler);
    APP_ERROR_CHECK(err_code);
    // Create Deep Sleep Timer
    err_code = app_timer_create(&sleep_timer_id, APP_TIMER_MODE_REPEATED, sleep_timer_int_handler);
    APP_ERROR_CHECK(err_code);
    // Create wakeup Timer
    err_code = app_timer_create(&wakeup_timer_id, APP_TIMER_MODE_SINGLE_SHOT, wakeup_timer_int_handler);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for starting timers.
*/
void measurement_timer_start(void)
{		
    uint32_t err_code,measurement_timer;
		if(config[0]==0x01)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL1;
		else if(config[0]==0x02)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL2;
		else if(config[0]==0x03)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL3;
		else if(config[0]==0x04)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL4;
		else if(config[0]==0x05)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL5;
		else if(config[0]==0x06)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL6;
		else if(config[0]==0x07)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL7;
		else if(config[0]==0x08)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL8;
		else if(config[0]==0x09)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL9;
		else if(config[0]==0x0A)
			measurement_timer = MEASUREMENT_TIMER_INTERVAL10;
		else
			measurement_timer = MEASUREMENT_TIMER_INTERVAL5;
		err_code = app_timer_start(measurement_timer_id, measurement_timer, NULL);
		APP_ERROR_CHECK(err_code);
}

void measurement_timer_stop(void)
{
    uint32_t err_code;
		err_code = app_timer_stop(measurement_timer_id);
		APP_ERROR_CHECK(err_code);
}

/* 	Timer Interrupt Handler for BMX055 calls
* 	if Bluetooth is connected, it starts the measurement
*       by setting run and starts the wakeup_timer for next measurement
*/
void measurement_timer_int_handler(void * p_context)
{ 
	if(connected == true)
	{
		run = true;
                babysitter_trigger++;
                wakeup_timer_start();
	}
	//sleep_cnt++;
	
}

void wakeup_timer_int_handler(void * p_context)
{
  wakeup = true;
}

void wakeup_timer_start(void)
{
    uint32_t err_code,wakeup_timer;
    if(config[0]==0x01)
            wakeup_timer = WAKEUP_TIMER_INTERVAL1;
    else if(config[0]==0x02)
            wakeup_timer = WAKEUP_TIMER_INTERVAL2;
    else if(config[0]==0x03)
            wakeup_timer = WAKEUP_TIMER_INTERVAL3;
    else if(config[0]==0x04)
            wakeup_timer = WAKEUP_TIMER_INTERVAL4;
    else if(config[0]==0x05)
            wakeup_timer = WAKEUP_TIMER_INTERVAL5;
    else if(config[0]==0x06)
            wakeup_timer = WAKEUP_TIMER_INTERVAL6;
    else if(config[0]==0x07)
            wakeup_timer = WAKEUP_TIMER_INTERVAL7;
    else if(config[0]==0x08)
            wakeup_timer = WAKEUP_TIMER_INTERVAL8;
    else if(config[0]==0x09)
            wakeup_timer = WAKEUP_TIMER_INTERVAL9;
    else if(config[0]==0x0A)
            wakeup_timer = WAKEUP_TIMER_INTERVAL10;
    else
            wakeup_timer = WAKEUP_TIMER_INTERVAL10;
    err_code = app_timer_start(wakeup_timer_id, wakeup_timer, NULL);
		APP_ERROR_CHECK(err_code);
}

void wakeup_timer_stop(void)
{
    uint32_t err_code;
		err_code = app_timer_stop(wakeup_timer_id);
		APP_ERROR_CHECK(err_code);
}

void sleep_timer_int_handler(void * p_context)
{
	get_pres = true;	
}

void sleep_timer_start(void)
{
    uint32_t err_code;
		err_code = app_timer_start(sleep_timer_id,SLEEP_TIMER_INTERVAL1,NULL);
		APP_ERROR_CHECK(err_code);
}

void sleep_timer_stop(void)
{
    uint32_t err_code;
		err_code = app_timer_stop(sleep_timer_id);
		APP_ERROR_CHECK(err_code);
}

bool bb_trigger()
{
  if( babysitter_trigger / config[0] == BABYSITTER_INTERVAL )
    { 
      babysitter_trigger = 0;
      return true;
    }
  else
    return false;
}

bool bb_ledOff()
{
  if( babysitter_trigger / config[0] == 5 )
    return true;
  else
    return false;
}