#include "services.h"

extern uint8_t config[10];			// config of timer / placeholder
extern uint8_t pressure_calib[19];
extern bool connected;					// Variable for device connection status
extern ble_dfu_t m_dfus;				// Bootloader Service
void ble_bmx_svc_on_ble_evt(ble_svc_bmx *svc, ble_evt_t * p_ble_evt)
{
    /* Switch to handle BLE Events und set connection status for notifications */
	switch (p_ble_evt->header.evt_id)
	{
			case BLE_GAP_EVT_CONNECTED:
					svc->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
					connected = true;
					break;
			case BLE_GAP_EVT_DISCONNECTED:
					svc->conn_handle = BLE_CONN_HANDLE_INVALID;
					connected = false;
					break;
			default:
					// No implementation needed.
					break;
	}
}
/**@brief Function for initializing services that will be used by the application.
 */
void bmx_svc_init(ble_svc_bmx *svc)
{
	uint32_t   err_code;
	ble_uuid_t        service_uuid;
	ble_uuid128_t     base_uuid = BLE_UUID_BASE;
	service_uuid.uuid = BLE_UUID_BMX_SVC;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                    &service_uuid,
                                    &svc->service_handle);
	APP_ERROR_CHECK(err_code);
	
	bmx_add_chars(svc);
}

void bmx_conf_svc_init(ble_svc_bmx_conf *svc)
{
	uint32_t   err_code;
	ble_uuid_t        service_uuid;
	ble_uuid128_t     base_uuid = BLE_UUID_BASE;
	service_uuid.uuid = BLE_UUID_BMX_CONF;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
	APP_ERROR_CHECK(err_code);
	
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                    &service_uuid,
                                    &svc->service_handle);
	APP_ERROR_CHECK(err_code);
	
	bmx_conf_add_chars(svc);
	
	#ifdef BLE_DFU_APP_SUPPORT
    /** @snippet [DFU BLE Service initialization] */
    ble_dfu_init_t   dfus_init;

    // Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.revision      = DFU_REVISION;

    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);

    dfu_app_reset_prepare_set(reset_prepare);
    dfu_app_dm_appl_instance_set(m_app_handle);
    /** @snippet [DFU BLE Service initialization] */
#endif // BLE_DFU_APP_SUPPORT
}


uint32_t bmx_conf_add_chars(ble_svc_bmx_conf *svc)
{
	// Add a custom characteristic UUID
    uint32_t            err_code;
    ble_uuid_t          gyro_conf_uuid,acc_conf_uuid,mag_conf_uuid,timer_conf_uuid,drop_conf_uuid;
    ble_uuid128_t       base_uuid = BLE_UUID_BASE;							// BASE-ID
    gyro_conf_uuid.uuid      = BLE_UUID_GYRO_CONF_UUID;					// GYRO-UUID
    acc_conf_uuid.uuid      = BLE_UUID_ACC_CONF_UUID;						// ACC-UUID
    mag_conf_uuid.uuid      = BLE_UUID_MAG_CONF_UUID;						// MAG-UUID
    timer_conf_uuid.uuid		= BLE_UUID_TIMER_CONF_UUID;					// Timer-UUID
    drop_conf_uuid.uuid   = BLE_UUID_DROP_CONF_UUID;
    /* Register Char-UUIDs to Service */
    err_code = sd_ble_uuid_vs_add(&base_uuid, &gyro_conf_uuid.type);	
    err_code = sd_ble_uuid_vs_add(&base_uuid, &acc_conf_uuid.type);
    err_code = sd_ble_uuid_vs_add(&base_uuid, &mag_conf_uuid.type);
    err_code = sd_ble_uuid_vs_add(&base_uuid, &timer_conf_uuid.type);
    err_code = sd_ble_uuid_vs_add(&base_uuid, &drop_conf_uuid.type);
    APP_ERROR_CHECK(err_code);
    /* Add read/write properties to characteristic (as seen by Client) */
    ble_gatts_char_md_t gyro_conf_char_md,acc_conf_char_md,mag_conf_char_md,timer_conf_char_md,drop_conf_char_md;
    memset(&gyro_conf_char_md, 0, sizeof(gyro_conf_char_md));
    memset(&acc_conf_char_md, 0, sizeof(acc_conf_char_md));
    memset(&mag_conf_char_md, 0, sizeof(mag_conf_char_md));
    memset(&timer_conf_char_md, 0, sizeof(timer_conf_char_md));
    memset(&drop_conf_char_md, 0, sizeof(drop_conf_char_md));

    gyro_conf_char_md.char_props.read = 1;
    gyro_conf_char_md.char_props.write = 1;
    acc_conf_char_md.char_props.read = 1;
    acc_conf_char_md.char_props.write = 1;
    mag_conf_char_md.char_props.read = 1;
    mag_conf_char_md.char_props.write = 1;
    timer_conf_char_md.char_props.read = 1;
    timer_conf_char_md.char_props.write = 1;
    drop_conf_char_md.char_props.read = 1;
    drop_conf_char_md.char_props.write = 1;
    /* Configuring Client Characteristic Configuration Descriptor metadata and add to char_md structure
       Setting Permissions for Chars */
    ble_gatts_attr_md_t gyro_conf_cccd_md,acc_conf_cccd_md,mag_conf_cccd_md,timer_conf_cccd_md,drop_conf_cccd_md;
    /* Initialize everything with default */
    memset(&gyro_conf_cccd_md, 0, sizeof(gyro_conf_cccd_md));
    memset(&acc_conf_cccd_md, 0, sizeof(acc_conf_cccd_md));
    memset(&mag_conf_cccd_md, 0, sizeof(mag_conf_cccd_md));
    memset(&timer_conf_cccd_md, 0, sizeof(timer_conf_cccd_md));
    memset(&drop_conf_cccd_md, 0, sizeof(drop_conf_cccd_md));
    /* Set Read and Write Permissions for services in general */
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&gyro_conf_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&gyro_conf_cccd_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&acc_conf_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&acc_conf_cccd_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&mag_conf_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&mag_conf_cccd_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&timer_conf_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&timer_conf_cccd_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&drop_conf_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&drop_conf_cccd_md.write_perm);
    /* Store CCCDs in Stack, allow notifications, set descriptions */
    static char gyro[] = "GyroCFG";
    static char acc[] = "AccCFG";
    static char mag[] = "MagCFG";
    static char timer[] = "TimerCFG";
    static char drop[] = "DropCFG";
    gyro_conf_cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    
    gyro_conf_char_md.p_cccd_md           = &gyro_conf_cccd_md;
    gyro_conf_char_md.char_props.notify   = 0;
    gyro_conf_char_md.char_user_desc_size = strlen(gyro);
    gyro_conf_char_md.char_user_desc_max_size = strlen(gyro);
    gyro_conf_char_md.p_char_user_desc		 = (uint8_t *) gyro;

    acc_conf_cccd_md.vloc                 = BLE_GATTS_VLOC_STACK;    
    acc_conf_char_md.p_cccd_md            = &acc_conf_cccd_md;
    acc_conf_char_md.char_props.notify    = 0;
    acc_conf_char_md.char_user_desc_size = strlen(acc);
    acc_conf_char_md.char_user_desc_max_size = strlen(acc);
    acc_conf_char_md.p_char_user_desc		 = (uint8_t *) acc;

    mag_conf_cccd_md.vloc                 = BLE_GATTS_VLOC_STACK;    
    mag_conf_char_md.p_cccd_md            = &mag_conf_cccd_md;
    mag_conf_char_md.char_props.notify    = 0;
    mag_conf_char_md.char_user_desc_size = strlen(mag);
    mag_conf_char_md.char_user_desc_max_size = strlen(mag);
    mag_conf_char_md.p_char_user_desc		 = (uint8_t *) mag;

    timer_conf_cccd_md.vloc                 = BLE_GATTS_VLOC_STACK;    
    timer_conf_char_md.p_cccd_md            = &timer_conf_cccd_md;
    timer_conf_char_md.char_props.notify    = 0;
    timer_conf_char_md.char_user_desc_size = strlen(timer);
    timer_conf_char_md.char_user_desc_max_size = strlen(timer);
    timer_conf_char_md.p_char_user_desc		 = (uint8_t *) timer;

    drop_conf_cccd_md.vloc                 = BLE_GATTS_VLOC_STACK;    
    drop_conf_char_md.p_cccd_md            = &drop_conf_cccd_md;
    drop_conf_char_md.char_props.notify    = 0;
    drop_conf_char_md.char_user_desc_size = strlen(drop);
    drop_conf_char_md.char_user_desc_max_size = strlen(drop);
    drop_conf_char_md.p_char_user_desc		 = (uint8_t *) drop;

    /* Configure the attribute metadata, store in Stack */
    ble_gatts_attr_md_t gyro_conf_attr_md,acc_conf_attr_md,mag_conf_attr_md,timer_conf_attr_md,drop_conf_attr_md;
    memset(&gyro_conf_attr_md, 0, sizeof(gyro_conf_attr_md));  
    memset(&acc_conf_attr_md, 0, sizeof(acc_conf_attr_md)); 
    memset(&mag_conf_attr_md, 0, sizeof(mag_conf_attr_md)); 
    memset(&timer_conf_attr_md, 0, sizeof(timer_conf_attr_md));
    memset(&drop_conf_attr_md, 0, sizeof(drop_conf_attr_md));
    gyro_conf_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    acc_conf_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    mag_conf_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    timer_conf_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    drop_conf_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    // Set read/write security levels to characteristics, atm open
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&gyro_conf_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&gyro_conf_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&acc_conf_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&acc_conf_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&mag_conf_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&mag_conf_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&timer_conf_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&timer_conf_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&drop_conf_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&drop_conf_attr_md.write_perm);
    // Configure the characteristic value attribute
    ble_gatts_attr_t    attr_gyro_conf_value,attr_acc_conf_value,attr_mag_conf_value,attr_timer_conf_value,attr_drop_conf_value;
    memset(&attr_gyro_conf_value, 0, sizeof(attr_gyro_conf_value));
    memset(&attr_acc_conf_value, 0, sizeof(attr_acc_conf_value));
    memset(&attr_mag_conf_value, 0, sizeof(attr_mag_conf_value));
    memset(&attr_timer_conf_value, 0, sizeof(attr_timer_conf_value));
    memset(&attr_drop_conf_value, 0, sizeof(attr_drop_conf_value));
    attr_gyro_conf_value.p_uuid      = &gyro_conf_uuid;
    attr_gyro_conf_value.p_attr_md   = &gyro_conf_attr_md;
    
    attr_acc_conf_value.p_uuid      = &acc_conf_uuid;
    attr_acc_conf_value.p_attr_md   = &acc_conf_attr_md;
    
    attr_mag_conf_value.p_uuid      = &mag_conf_uuid;
    attr_mag_conf_value.p_attr_md   = &mag_conf_attr_md;
    
    attr_timer_conf_value.p_uuid      = &timer_conf_uuid;
    attr_timer_conf_value.p_attr_md   = &timer_conf_attr_md;

    attr_drop_conf_value.p_uuid      = &drop_conf_uuid;
    attr_drop_conf_value.p_attr_md   = &drop_conf_attr_md;
    
    /* Set characteristic length in number of bytes */
    attr_gyro_conf_value.max_len     = 1;
    attr_gyro_conf_value.init_len    = 1;
    uint8_t value[1]            = {0x00};									// 2000�/s		in ASCII
    attr_gyro_conf_value.p_value     = value;
    uint8_t value_acc[1]            = {0x03};							// 2g Range	in ASCII
    attr_acc_conf_value.max_len     = 1;
    attr_acc_conf_value.init_len    = 1;
    attr_acc_conf_value.p_value     = value_acc;
    uint8_t value_mag[1]             = {0x00};						// XY = 3, Z = 3
    attr_mag_conf_value.max_len     = 1;
    attr_mag_conf_value.init_len    = 1;
    attr_mag_conf_value.p_value     = value_mag;
    uint8_t value_timer[1]             = {0x0A};					// 10 measurements/second
    attr_timer_conf_value.max_len     = 1;
    attr_timer_conf_value.init_len    = 1;
    attr_timer_conf_value.p_value     = value_timer;

    uint8_t value_drop[1]             = {0x00};					// No Drop detection
    attr_timer_conf_value.max_len     = 1;
    attr_timer_conf_value.init_len    = 1;
    attr_timer_conf_value.p_value     = value_timer;
    /* Add characteristics to the service */
    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &gyro_conf_char_md,
                       &attr_gyro_conf_value,
                       &svc->gyro_conf_handle);
    APP_ERROR_CHECK(err_code);
    
    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &acc_conf_char_md,
                       &attr_acc_conf_value,
                       &svc->acc_conf_handle);
    APP_ERROR_CHECK(err_code);
    
    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &mag_conf_char_md,
                       &attr_mag_conf_value,
                       &svc->mag_conf_handle);
    APP_ERROR_CHECK(err_code);
    
    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &timer_conf_char_md,
                       &attr_timer_conf_value,
                       &svc->timer_conf_handle);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &drop_conf_char_md,
                       &attr_drop_conf_value,
                       &svc->drop_conf_handle);
    APP_ERROR_CHECK(err_code);

    return NRF_SUCCESS;
}

uint32_t bmx_add_chars(ble_svc_bmx *svc)
{
	// Add a custom characteristic UUID
    uint32_t            err_code;
    ble_uuid_t          sensors_uuid,bmp_calib_uuid;
    ble_uuid128_t       base_uuid = BLE_UUID_BASE;							// BASE-ID
    sensors_uuid.uuid			=	BLE_UUID_SENSORS_UUID;
    bmp_calib_uuid.uuid = BLE_UUID_PRESSURE_CALIB_UUID;
    // Register Char-UUIDs to Service
    err_code = sd_ble_uuid_vs_add(&base_uuid, &sensors_uuid.type);
    APP_ERROR_CHECK(err_code);
    err_code = sd_ble_uuid_vs_add(&base_uuid, &bmp_calib_uuid.type);
    APP_ERROR_CHECK(err_code);
    // Add read/write properties to characteristic (seen by Client)
    ble_gatts_char_md_t sensors_char_md,bmp_calib_char_md;
    memset(&sensors_char_md, 0, sizeof(sensors_char_md));
    memset(&bmp_calib_char_md, 0, sizeof(bmp_calib_char_md));
    sensors_char_md.char_props.read = 1;
    bmp_calib_char_md.char_props.read = 1;
    // Configuring Client Characteristic Configuration Descriptor metadata and add to char_md structure
    // Setting Permissions for Chars
    ble_gatts_attr_md_t sensors_cccd_md,bmp_calib_cccd_md;
    // Initialize everything with default
    memset(&sensors_cccd_md, 0, sizeof(sensors_cccd_md));
    memset(&bmp_calib_cccd_md, 0, sizeof(bmp_calib_cccd_md));
    // Set Read and Write Permissions for CCCD
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sensors_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sensors_cccd_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bmp_calib_cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bmp_calib_cccd_md.write_perm);
    // Store CCCDs in Stack, allow notifications
    static char sensors[] = "Sensors";
    static char bmp_calib[]= "BMP_Calib";

    
    sensors_cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    
    sensors_char_md.p_cccd_md           = &sensors_cccd_md;
    sensors_char_md.char_props.notify   = 1;
    sensors_char_md.char_user_desc_size = strlen(sensors);
    sensors_char_md.char_user_desc_max_size = strlen(sensors);
    sensors_char_md.p_char_user_desc		 = (uint8_t *) sensors;

    bmp_calib_cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    
    bmp_calib_char_md.p_cccd_md           = &bmp_calib_cccd_md;
    bmp_calib_char_md.char_props.notify   = 1;
    bmp_calib_char_md.char_user_desc_size = strlen(bmp_calib);
    bmp_calib_char_md.char_user_desc_max_size = strlen(bmp_calib);
    bmp_calib_char_md.p_char_user_desc		 = (uint8_t *) bmp_calib;

    // Configure the attribute metadata, store in Stack
    ble_gatts_attr_md_t sensors_attr_md,bmp_calib_attr_md;

    memset(&sensors_attr_md, 0, sizeof(sensors_attr_md)); 
    memset(&bmp_calib_attr_md, 0, sizeof(bmp_calib_attr_md)); 

    sensors_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    bmp_calib_attr_md.vloc        = BLE_GATTS_VLOC_STACK;
    // Set read/write security levels to characteristics, atm open
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sensors_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bmp_calib_attr_md.read_perm);
    // Configure the characteristic value attribute
    ble_gatts_attr_t    attr_sensors_value,attr_calib_value;

    memset(&attr_sensors_value, 0, sizeof(attr_sensors_value));
    memset(&attr_calib_value, 0, sizeof(attr_calib_value));

    
    attr_sensors_value.p_uuid      = &sensors_uuid;
    attr_sensors_value.p_attr_md   = &sensors_attr_md;

    attr_calib_value.p_uuid      = &bmp_calib_uuid;
    attr_calib_value.p_attr_md   = &bmp_calib_attr_md;
    
    // Set characteristic length in number of bytes

    uint8_t value[2]            = {0xFF,0xFF};

    
    attr_sensors_value.max_len     = 17;
    attr_sensors_value.init_len    = 2;
    attr_sensors_value.p_value     = value;

    attr_calib_value.max_len     = 18;
    attr_calib_value.init_len    = 18;
    attr_calib_value.p_value     = pressure_calib;
    // Add characteristics to the service
		
    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &sensors_char_md,
                       &attr_sensors_value,
                       &svc->sensors_handle);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gatts_characteristic_add(svc->service_handle,
                       &bmp_calib_char_md,
                       &attr_calib_value,
                       &svc->bmp_calib_handle);
    APP_ERROR_CHECK(err_code);
		
    return NRF_SUCCESS;
}

/*  Updates the Sensor Characteristic
*/
void sensors_char_update(ble_svc_bmx *bmx_svc, uint8_t *sensors_val)
{
  // JUST SEND NOTIFICATION IF CONNECTION IS ESTABLISHED - OTHERWISE ERROR FROM STACK!
	if (bmx_svc->conn_handle != BLE_CONN_HANDLE_INVALID)
	{	
            uint16_t               len = 17;
            ble_gatts_hvx_params_t hvx_params;
            memset(&hvx_params, 0, sizeof(hvx_params));
            // GET HANDLE
            hvx_params.handle = bmx_svc->sensors_handle.value_handle;
            hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
            hvx_params.offset = 0;
            hvx_params.p_len  = &len;
            hvx_params.p_data = (uint8_t*)sensors_val;  
            // STORE VALUE AND SEND NOTIFICATION
            sd_ble_gatts_hvx(bmx_svc->conn_handle, &hvx_params);
	
	}
	
}
/*  Updates the BMP Calibration Characteristic
*/
void bmp_calib_update(ble_svc_bmx *bmx_svc, uint8_t *sensors_val)
{
  // JUST SEND NOTIFICATION IF CONNECTION IS ESTABLISHED - OTHERWISE ERROR FROM STACK!
	if (bmx_svc->conn_handle != BLE_CONN_HANDLE_INVALID)
	{	
		uint16_t               len = 18;
		ble_gatts_hvx_params_t hvx_params;
		memset(&hvx_params, 0, sizeof(hvx_params));
		// GET HANDLE
		hvx_params.handle = bmx_svc->bmp_calib_handle.value_handle;
		hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
		hvx_params.offset = 0;
		hvx_params.p_len  = &len;
		hvx_params.p_data = (uint8_t*)sensors_val;  
		// STORE VALUE AND SEND NOTIFICATION
		sd_ble_gatts_hvx(bmx_svc->conn_handle, &hvx_params);
	
	}
	
}

/*  Parses writing to a characteristic to config
*   Update triggers on next measurement
*/
void set_config(ble_evt_t * p_ble_evt)
{ uint8_t *debug = p_ble_evt->evt.gatts_evt.params.write.data;
  ble_uuid_t uuid = p_ble_evt->evt.gatts_evt.params.write.uuid;
  if(p_ble_evt->header.evt_id == BLE_GATTS_EVT_WRITE)
  {
  if (uuid.uuid == BLE_UUID_TIMER_CONF_UUID)
    {
      config[0]=*debug;
      
    }
   else if (uuid.uuid == BLE_UUID_GYRO_CONF_UUID)
    {
      config[1]=*debug;
    }
   else if (uuid.uuid == BLE_UUID_ACC_CONF_UUID)
    {
      config[2]=*debug;
    }
    else if (uuid.uuid == BLE_UUID_DROP_CONF_UUID)
    {
      config[3]=*debug;
    }
  }
}
