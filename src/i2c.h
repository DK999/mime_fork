#include "nrf_drv_twi.h"
#include "app_util_platform.h"
#include "nrf_delay.h"
#include "segger_rtt.h"

#define SDA_PIN 				11
#define SCL_PIN 				12
#define I2C_FREQ 				NRF_TWI_FREQ_400K
#define I2C_IRQ 				APP_IRQ_PRIORITY_LOW



// Parameters for the capacity() function, to specify which capacity to read
typedef enum {
	REMAIN,     // Remaining Capacity (DEFAULT)
	FULL,       // Full Capacity
	AVAIL,      // Available Capacity
	AVAIL_FULL, // Full Available Capacity
	REMAIN_F,   // Remaining Capacity Filtered
	REMAIN_UF,  // Remaining Capacity Unfiltered
	FULL_F,     // Full Capacity Filtered
	FULL_UF,    // Full Capacity Unfiltered
	DESIGN      // Design Capacity
} capacity_measure;

void i2c_handler(nrf_drv_twi_evt_t const * p_event, void * p_context);
void wait_for_I2C(void);
void i2c_enable (void);
void i2c_disable (void);
void i2c_init (void);
bool i2c_send(uint8_t address, uint8_t *data, uint8_t length);
bool i2c_read(uint8_t address, uint8_t reg, uint8_t *data, uint8_t length);