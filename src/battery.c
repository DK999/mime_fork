#include "battery.h"

static nrf_saadc_value_t m_buffer_pool[2][SAMPLES_IN_BUFFER];
extern uint8_t batt_val[2];
/*  SAADC CALLBACK function
    Gets called everytime a SAADC Event occurs 
    */
void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{   // ##### IF MEASUREMENT DONE #####
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {		
        ret_code_t err_code;
        // ##### CONVERT ADC DATA #####
        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);
        // ##### STORE IN VARIABLE #####
	batt_val[0]=	p_event->data.done.p_buffer[0];
        // ##### SHUT DOWN SAADC FOR POWER SAVING #####
        nrf_drv_saadc_uninit();
        NRF_SAADC->INTENCLR = (SAADC_INTENCLR_END_Clear << SAADC_INTENCLR_END_Pos);
        NVIC_ClearPendingIRQ(SAADC_IRQn);
    }
    
}
/* Initializes the ADC Channel and selects Buffer */
void saadc_init(void)
{  
    ret_code_t err_code;
    // ##### DEFINE CALLBACK! #####
    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    // ##### SET INPUT AND CONFIG #####
    nrf_saadc_channel_config_t channel_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_VDD);	
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_saadc_channel_init(0, &channel_config);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0],SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
    
    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1],SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
    
}

void saadc_sampling_trigger(void)
{   
    ret_code_t err_code;
    saadc_init();
    //Event handler is called immediately after conversion is finished.
    err_code = nrf_drv_saadc_sample(); // Check error
    APP_ERROR_CHECK(err_code);
}
