#include "spi.h"

#define BMX_MAG_ID 			0xC0
#define BMX_MAG_DATA 			0xC2
#define BMX_MAG_PWR_CTRL1               0x4B
#define BMX_MAG_PWR_CTRL2               0x4C
#define BMX_MAG_REP_XY                  0x51
#define BMX_MAG_REP_Z                   0x52
#define BMX_MAG_NORMAL_MODE             0x00
#define BMX_MAG_SLEEP_MODE              0x06
#define BMX_MAG_FORCE_MODE              0x02

#define BMX_ACC_BGW_RESET		0x14
#define BMX_ACC_SOFTRESET		0xB6
#define BMX_ACC_PMU_RANGE		0x0F
#define BMX_ACC_STD_RANGE		0x03
#define BMX_ACC_PMU_BDWTH		0x10
#define BMX_ACC_STD_BDWTH		0x0F
#define BMX_ACC_PMU_LPW1		0x11
#define BMX_ACC_LPW1_NRML		0x00
#define BMX_ACC_LPW1_SPND		0x80
#define BMX_ACC_PMU_LPW2		0x12
#define BMX_ACC_LPW2_NO_SLEEP           0x00
#define BMX_ACC_HBW_CTRL		0x13
#define BMX_ACC_HBW_DATA_H              0x80

#define BMX_GYR_PMU_LPM1		0x11
#define BMX_GYR_LPM1_NRML		0x00
#define BMX_GYR_LPM1_SPND		0x80

#define BMX_ACC_INT_EN1                 0x17
#define BMX_ACC_EN_HIGH_G               0x07
#define BMX_ACC_HIGH_DURATION_REG       0x25
#define BMX_ACC_HIGH_DURATION           0x0F
#define BMX_ACC_HIGH_THRESHOLD_REG      0x26
#define BMX_ACC_HIGH_THRESHOLD          0x80


void bmx_init_gyro(void);
void bmx_init_acc(void);
void bmx_init_mag(void);
void bmx_init_temp(void);
void bmx_get_accgyro(uint8_t *rx,uint8_t type);
void bmx_get_mag(uint8_t *rx);
void bmx_get_temp(uint8_t *rx);
void bmx_set_gyro_res(uint8_t range);
void bmx_set_acc_res(uint8_t res);
void bmx_set_mag_res(uint8_t *res);
void bmx_set_sleep();
void bmx_wakeup();
void bmx_set_acc_int();